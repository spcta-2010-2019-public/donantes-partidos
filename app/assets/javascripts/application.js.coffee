# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

#= require 'jquery'
#= require 'jquery_ujs'
#= require 'jquery-ui'
# require 'bootstrap-sprockets'
# require 'modernizr'
#= require 'main'
# require 'chart.min'
# require 'jquery.validate.min'
# require 'error-displayer'
#= require 'jssocials'
#= require 'jssocials.shares'

# require jquery.mask

$ ->
  $('.js-politicals-select').select2
    placeholder: 'Partidos políticos'
    allowClear: true
    width: '100%'
  $('.js-years-select').select2
    placeholder: 'Años'
    allowClear: true
    width: '100%'
