class PagesController < ApplicationController
  layout 'application'

  def index
    # unless params[:q].blank?
    #   redirect_to root_url
    # end

    @politicals = Political.order(:acronym)
    @q = Donation.order('year DESC, political_id ASC, amount DESC').includes(:political).ransack(params[:q])
    @donations = @q.result.paginate(:page => params[:page], :per_page => 10)

  end
end
