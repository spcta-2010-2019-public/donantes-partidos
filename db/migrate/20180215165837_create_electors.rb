class CreateElectors < ActiveRecord::Migration[5.0]
  def change
    create_table :electors do |t|
      t.integer  :cod_depto_domic
      t.string :nom_depto
      t.integer :cod_munic_domic
      t.text :nom_munic
      t.integer :cod_sector
      t.text :nom_sector
      t.string :apellidos
      t.string :nombres
      t.string :dui
      t.integer :jrv
      t.integer :correlativo
      #t.timestamps
    end
  end
end

# psql -U postgres -d dondevotar_development -c "COPY electors(cod_depto_domic,nom_depto,cod_munic_domic,nom_munic,cod_sector,nom_sector,apellidos,nombres,dui,jrv,correlativo) from '/home/danilo/projects/duis/small.csv'"

# psql -U postgres -d dondevotar_production -c "COPY electors(cod_depto_domic,nom_depto,cod_munic_domic,nom_munic,cod_sector,nom_sector,apellidos,nombres,dui,jrv,correlativo) from '/tmp/ELECTOR.csv' CSV HEADER"
