class AddIndexToElectors < ActiveRecord::Migration[5.0]
  def change
    add_index :electors, :dui
  end
end
