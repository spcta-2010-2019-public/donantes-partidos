class CreatePoliticals < ActiveRecord::Migration[5.0]
  def change
    create_table :politicals do |t|
      t.string :acronym, default: ''
      t.string :name
      t.timestamps
    end
  end
end
