class CreateDonations < ActiveRecord::Migration[5.0]
  def change
    create_table :donations do |t|
      t.integer :year, index: true
      t.references :political, index: true
      t.string :donor
      t.decimal :amount, precision: 15, scale: 2
      t.timestamps
    end
  end
end
