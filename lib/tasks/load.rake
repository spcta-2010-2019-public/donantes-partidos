require 'csv'
namespace :load do

  desc 'Carga de listado de donantes'
  task donantes: [:environment] do
    [
      "#{Rails.root.to_s}/db/csv/2006.csv",
      "#{Rails.root.to_s}/db/csv/2007.csv",
      "#{Rails.root.to_s}/db/csv/2008.csv",
      "#{Rails.root.to_s}/db/csv/2009.csv",
      "#{Rails.root.to_s}/db/csv/2010.csv",
      "#{Rails.root.to_s}/db/csv/2011.csv",
      "#{Rails.root.to_s}/db/csv/2012.csv",
      "#{Rails.root.to_s}/db/csv/2013.csv",
      "#{Rails.root.to_s}/db/csv/2014.csv",
      "#{Rails.root.to_s}/db/csv/2015.csv",
      "#{Rails.root.to_s}/db/csv/2016.csv",
      "#{Rails.root.to_s}/db/csv/2017.csv",
      "#{Rails.root.to_s}/db/csv/2018.csv",
    ].each do |csv_path|
      CSV.foreach(csv_path, headers: true, encoding:'utf-8', col_sep: ',') do |row|
        begin
          political = Political.where(name: row[1]).first_or_create
          Donation.create(year: row[0], political_id: political.id, donor: row[2], amount: row[3].gsub(',','.').strip)
        rescue
          puts "Hubo un error cargando: #{row.inspect}."
        end
      end
    end
  end

  desc 'Unificar partidos'
  task politicals: [:environment] do

    fmln = Political.where(name: "PARTIDO FRENTE FARABUNDO MARTI PARA LA LIBERACION NACIONAL").first
    fmln.update_column(:acronym, 'FMLN')

    pcn = Political.where(name: "PARTIDO DE CONCILIACION NACIONAL").first
    pcn.update_column(:acronym, 'PCN')

    pdc = Political.where(name: "PARTIDO DEMOCRATA CRISTIANO PDC").first
    pdc.update_column(:acronym, 'PDC')

    arena = Political.where(name: "PARTIDO ALIANZA REPUBLICANA NACIONALISTA").first
    arena.update_column(:acronym, 'ARENA')

    pln = Political.where(name: "PARTIDO DE LIBERACIÓN NACIONAL PLN").first
    pln.update_column(:acronym, 'PLN')

    pp = Political.where(name: "PARTIDO POPULAR").first
    pp.update_column(:acronym, 'PP')

    gana = Political.where(name: "PARTIDO GRAN ALIANZA POR LA UNIDAD NACIONAL").first
    gana.update_column(:acronym, 'GANA')

    psp = Political.where(name: "PARTIDO SALVADOREÑO PROGRESISTA (PSP)").first
    psp.update_column(:acronym, 'PSP')

    cd = Political.where(name: "CAMBIO DEMOCRÁTICO").first
    cd.update_column(:acronym, 'CD')

    ds = Political.where(name: "DEMOCRACIA SALVADOREÑA").first
    ds.update_column(:acronym, 'DS')

    ### PCN ALIASES

    obj = Political.where(name: "PARTIDO DE CONCILIACION NACIONAL/ PARTIDO DE CONCERTACIÓN NACIONAL (PCN)").first
    Donation.where(political_id: obj.id).update_all(political_id: pcn.id)
    obj.destroy

    obj = Political.where(name: "CONCERTACION NACIONAL").first
    Donation.where(political_id: obj.id).update_all(political_id: pcn.id)
    obj.destroy

    obj = Political.where(name: "PARTIDO DE CONCERTACIÓN NACIONAL").first
    Donation.where(political_id: obj.id).update_all(political_id: pcn.id)
    obj.destroy

    obj = Political.where(name: "PARTIDO DE CONCERTACION NACIONAL PCN").first
    Donation.where(political_id: obj.id).update_all(political_id: pcn.id)
    obj.destroy

    ### GANA ALIASES

    obj = Political.where(name: "PARTIDO GRAN ALIANZA UNIDAD NACIONAL").first
    Donation.where(political_id: obj.id).update_all(political_id: gana.id)
    obj.destroy

    obj = Political.where(name: "GRAN ALIANZA POR LA UNIDAD NACIONAL GANA").first
    Donation.where(political_id: obj.id).update_all(political_id: gana.id)
    obj.destroy




  end

end
