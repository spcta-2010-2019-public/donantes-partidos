Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #domain = 'avanzado.calculatupension.info' if Rails.env.production?
  domain = 'localhost.com'                  if Rails.env.development?
  root to: 'pages#index'
  #get '/centro-de-votacion' => 'pages#show'
end
