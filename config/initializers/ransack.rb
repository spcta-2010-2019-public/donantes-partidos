# frozen_string_literal: true
#
Ransack.configure do |config|
  config.add_predicate 'acont',
                       arel_predicate: 'matches',
                       formatter: proc { |s| "%#{s.split.join('%')}%" },
                       validator: proc { |s| s.present? },
                       compounds: true,
                       type: :string
end
